package com.learnrest.dao;

import com.learnrest.dao.impl.UserDAOImpl;
import com.learnrest.model.Profile;
import com.learnrest.model.User;
import java.util.Date;
import javax.inject.Inject;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author fernando
 */
@RunWith(CdiRunner.class)
@AdditionalClasses(UserDAOImpl.class)
public class UserTest {

    private static final User root;

    static {
        root = new User("root", "senha10", User.UserType.ROOT, new Profile("Root Boy", new Date()));
    }

    @Inject
    private UserDAO userDAO;

    @Test
    public void cdiInjectionTest() {
        Assert.assertNotNull(userDAO);
    }

    @Test
    public void saveTest() {
        User found = userDAO.findByUsername(this.root.getUsername());
        if (found == null) {
            userDAO.save(root);
        }

        found = userDAO.findByUsername(root.getUsername());
        Assert.assertNotNull(found);
        Assert.assertNotNull(found.getId());
        Assert.assertNotNull(found.getProfile().getId());
    }
}
